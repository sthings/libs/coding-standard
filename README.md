# SmartThings coding standard

Code standard is based on [Consistence Coding Standard](https://github.com/consistence/coding-standard) and
[Slevomat Coding Standard](https://github.com/slevomat/coding-standard) with some modifications.

See [CodingStandard/ruleset.xml]() for more details.

## Usage in project

### Install

```
composer config repositories.sthings/coding-standard git git@gitlab.com:sthings/libs/coding-standard.git
composer require sthings/coding-standard
```

### Configure PHPCS

```
cp vendor/sthings/coding-standard/ruleset.xml.dist ruleset.xml
```
Don't forget to properly setup `SlevomatCodingStandard.Files.TypeNameMatchesFileName` rule - see [doc reference](https://github.com/slevomat/coding-standard#slevomatcodingstandardfilestypenamematchesfilename) - either define namespaces (desired) or disable sniff in your project ruleset.

### Configure IDE

Go to Settings > search for Inspections > search for PHP CodeSniffer validation:

Enable the inspection, select SmartThingsCodingStandard and apply changes.


[?] You may also need to setup phpcs executable path:

Go to Settings > search for CodeSniffer.

Configure Development environment (Local).
