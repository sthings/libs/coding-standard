<?php

declare (strict_types=1);

namespace SmartThingsCodingStandard\Helpers;

use PHP_CodeSniffer\Files\File;
use SlevomatCodingStandard\Helpers\Annotation;
use SlevomatCodingStandard\Helpers\AnnotationHelper;
use SlevomatCodingStandard\Helpers\FunctionHelper as SlevomatFunctionHelper;
use SlevomatCodingStandard\Helpers\TokenHelper;

class FunctionHelper extends SlevomatFunctionHelper
{

    public static function checkFunctionPointer(File $file, int $pointer): void
    {
        $functionPointer = $file->getTokens()[$pointer];
        if ($functionPointer['code'] !== T_FUNCTION) {
            throw new \Exception(
                sprintf(
                    'Only T_FUNCTION pointers are allowed, %s token received',
                    $functionPointer['type']
                )
            );
        }
    }

    public static function getFunctionBodyEndPoint(File $file, int $pointer): ?int
    {
        self::checkFunctionPointer($file, $pointer);

        $bracketLevel = 0;
        while ($pointer < $file->numTokens) {
            $scopeToken = $file->getTokens()[$pointer];

            if ($scopeToken['code'] === T_OPEN_CURLY_BRACKET) {
                $bracketLevel++;
            } elseif ($scopeToken['code'] === T_CLOSE_CURLY_BRACKET) {
                if (--$bracketLevel === 0) {
                    return $pointer;
                }
            }
            $pointer++;
        }
        return null;
    }

    public static function getMethodVisibilityPointer(File $file, int $pointer): ?int
    {
        self::checkFunctionPointer($file, $pointer);

        $tokens = $file->getTokens();
        $functionToken = $tokens[$pointer];

        $methodVisibilityPointer = TokenHelper::findPrevious($file, [T_PUBLIC, T_PRIVATE, T_PROTECTED], $pointer);
        if ($methodVisibilityPointer !== null) {

            $methodVisibilityToken = $tokens[$methodVisibilityPointer];
            if ($methodVisibilityToken['line'] === $functionToken['line']) {
                return $methodVisibilityPointer;
            }
        }
        return null;
    }

    public static function removeFunction(File $file, int $pointer): void
    {
        self::checkFunctionPointer($file, $pointer);

        $functionEndPointer = self::getFunctionBodyEndPoint($file, $pointer);
        $functionStartPointer = $pointer;
        $annotations = AnnotationHelper::getAnnotations($file, $pointer);

        $methodVisibilityPointer = self::getMethodVisibilityPointer($file, $functionStartPointer);
        if ($methodVisibilityPointer !== null) {
            $functionStartPointer = $methodVisibilityPointer;
        }

        $whiteSpacePointer = WhitespaceHelper::findWhitespaceBeginBeforePointer($file, $functionStartPointer);
        if ($whiteSpacePointer !== null) {
            $functionStartPointer = $whiteSpacePointer;
        }

        if (count($annotations) > 0) {
            foreach ($annotations as $annotationName => $annotationsByName) {
                foreach ($annotationsByName as $annotation) {
                    /** @var Annotation $annotation */
                    $functionStartPointer = min($functionStartPointer, $annotation->getStartPointer());
                }
            }
        }

        FixerHelper::removeTokens($file, $functionStartPointer, $functionEndPointer);
    }

}
