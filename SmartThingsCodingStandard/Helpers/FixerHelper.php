<?php

declare (strict_types=1);

namespace SmartThingsCodingStandard\Helpers;

use PHP_CodeSniffer\Files\File;

class FixerHelper
{

    public static function removeTokens(File $file, int $from, int $to): void
    {
        for ($i = $from; $i <= $to; $i++) {
            $file->fixer->replaceToken($i, '');
        }
    }
}
