<?php

declare (strict_types=1);

namespace SmartThingsCodingStandard\Helpers;

use PHP_CodeSniffer\Files\File;

class WhitespaceHelper
{

    public static function findWhitespaceBeginBeforePointer(File $file, int $pointer): ?int
    {
        $tokens = $file->getTokens();
        while ($pointer > 0) {
            $pointer--;
            if ($tokens[$pointer]['code'] !== T_WHITESPACE) {
                return ++$pointer;
            }
        }
        return null;
    }
}
