<?php

declare (strict_types=1);

namespace SmartThingsCodingStandard\Helpers;

use PHP_CodeSniffer\Files\File;
use SlevomatCodingStandard\Helpers\PropertyHelper as SlevomatPropertyHelper;
use SlevomatCodingStandard\Helpers\TokenHelper;

class PropertyHelper extends SlevomatPropertyHelper
{

    public static function isStaticProperty(File $file, int $pointer): bool
    {
        if (!self::isProperty($file, $pointer)) {
            return false;
        }

        $staticDeterminingPointer = TokenHelper::findPreviousExcluding(
            $file,
            array_merge(
                [
                    T_PRIVATE,
                    T_PROTECTED,
                    T_PUBLIC,
                    T_VAR,
                ],
                TokenHelper::$ineffectiveTokenCodes
            ),
            $pointer - 1
        );

        return $file->getTokens()[$staticDeterminingPointer]['code'] === T_STATIC;
    }

}
