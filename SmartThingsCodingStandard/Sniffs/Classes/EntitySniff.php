<?php

declare (strict_types=1);

namespace SmartThingsCodingStandard\Sniffs\Classes;

use SmartThingsCodingStandard\Helpers\FixerHelper;
use SmartThingsCodingStandard\Helpers\FunctionHelper;
use SmartThingsCodingStandard\Helpers\PropertyHelper;
use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use SlevomatCodingStandard\Helpers\Annotation;
use SlevomatCodingStandard\Helpers\AnnotationHelper;
use SlevomatCodingStandard\Helpers\ClassHelper;
use SlevomatCodingStandard\Helpers\DocCommentHelper;
use SlevomatCodingStandard\Helpers\TokenHelper;

class EntitySniff implements Sniff
{

    public const METHOD_FLUENT_SETTER = 'MethodFluentSetter';
    public const METHOD_ID_SETTER = 'MethodIdSetter';
    public const METHOD_ID_GETTER_NOTNULL = 'MethodIdGetterNotNull';
    public const NULLABLE_PROPERTIES_DEFINED_IN_CONSTRUCTOR = 'NullablePropertiesDefinedInConstructor';

    private const NULLABLE_VAR_ANNOTATION_REGEX = '#^(.*\|)?null(\|.*)?$#i';

    /**
     * @var string
     */
    private $classFullyQualifiedName;

    /**
     * @var string
     */
    private $className;

    /**
     * @var bool
     */
    private $entityClass = false;

    /**
     * @var string[]
     */
    private $nonNullableProperties = [];

    /**
     * @return int[]
     */
    public function register(): array
    {
        return [
            T_FUNCTION,
            T_METHOD_C,
            T_CLASS,
            T_DOC_COMMENT_OPEN_TAG,
        ];
    }

    /**
     * @param int $pointer
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint
     */
    public function process(File $file, $pointer): void
    {
        $tokens = $file->getTokens();

        $token = $tokens[$pointer];

        if ($token['code'] === T_CLASS) {
            $this->className = ClassHelper::getName($file, $pointer);
            $this->classFullyQualifiedName = ClassHelper::getFullyQualifiedName($file, $pointer);
            $this->entityClass = false;

            $classAnnotationStartPointer = TokenHelper::findPrevious($file, [T_DOC_COMMENT_OPEN_TAG], $pointer);

            if ($classAnnotationStartPointer !== null) {
                $entityAnnotation = AnnotationHelper::getAnnotationsByName($file, $classAnnotationStartPointer, '@ORM\Entity');

                $this->entityClass = count($entityAnnotation) > 0;
            } elseif (substr($this->className, -2) === 'VO') {
                $this->entityClass = true;
            }

            $this->nonNullableProperties = [];
            return;
        }

        if (!$this->entityClass) {
            return;
        }

        //non-nullable properties detection
        if ($token['code'] === T_DOC_COMMENT_OPEN_TAG && DocCommentHelper::getDocComment($file, $pointer)) {
            $this->extractNonNullableProperties($file, $pointer);
            return;
        }
        $functionName = FunctionHelper::getName($file, $pointer);
        $parameterNames = FunctionHelper::getParametersNames($file, $pointer);

        //Fluent setter detection
        if (preg_match('#set[A-Za-z0-9]#', $functionName) && count($parameterNames) === 1) {
            $this->returnsSelf($file, $pointer);
            if ($functionName === 'setId') {
                $fix = $file->addFixableError(
                    sprintf('setId methods are not allowed'),
                    $pointer,
                    self::METHOD_ID_SETTER
                );

                if ($fix) {
                    FunctionHelper::removeFunction($file, $pointer);
                }
            }
        }

        //getId not null
        if ($functionName === 'getId') {
            $this->returnsNullable($file, $pointer);
        }

        if ($functionName === '__construct') {
            $missingProperties = array_diff($this->nonNullableProperties, $parameterNames);

            if (count($missingProperties) > 0) {
                $file->addError(
                    sprintf('Non-nullable properties should be set in constructor. Missing properties: %s', implode(', ', $missingProperties)),
                    $pointer,
                    self::NULLABLE_PROPERTIES_DEFINED_IN_CONSTRUCTOR
                );
            }
        }
    }

    private function returnsSelf(File $file, int $pointer): void
    {
        $functionName = FunctionHelper::getName($file, $pointer);
        $annotation = FunctionHelper::findReturnAnnotation($file, $pointer);
        $typeHint = FunctionHelper::findReturnTypeHint($file, $pointer);

        $possibleReturns = [
            'self',
            '$this',
            $this->className,
            $this->classFullyQualifiedName,
        ];

        $fix = false;
        if ($annotation !== null && in_array($annotation->getContent(), $possibleReturns)) {
            $fix = $file->addFixableError(
                sprintf('Annotation suggests setter method %s is fluent', $functionName),
                $pointer,
                self::METHOD_FLUENT_SETTER
            );

            if ($fix) {
                $file->fixer->beginChangeset();
                FixerHelper::removeTokens($file, $annotation->getStartPointer(), $annotation->getEndPointer());
            }
        } elseif ($typeHint !== null && in_array($typeHint->getTypeHint(), $possibleReturns)) {
            $fix = $file->addFixableError(
                sprintf('Return type hint suggests setter method %s is fluent', $functionName),
                $pointer,
                self::METHOD_FLUENT_SETTER
            );

            if ($fix) {
                $file->fixer->beginChangeset();
                $typeHintStartPointer = TokenHelper::findPrevious($file, [T_COLON], $typeHint->getStartPointer());

                FixerHelper::removeTokens($file, $typeHintStartPointer, $typeHint->getEndPointer());
            }
        }

        if ($fix) {
            $methodEndPointer = FunctionHelper::getFunctionBodyEndPoint($file, $pointer);
            $returnPointer = TokenHelper::findPrevious($file, [T_RETURN], $methodEndPointer);
            FixerHelper::removeTokens($file, $returnPointer, $methodEndPointer - 1);

            $file->fixer->endChangeset();
        }
    }

    private function returnsNullable(File $file, int $pointer): void
    {
        if (!FunctionHelper::returnsValue($file, $pointer)) {
            return;
        }
        $annotation = FunctionHelper::findReturnAnnotation($file, $pointer);
        $typeHint = FunctionHelper::findReturnTypeHint($file, $pointer);
        $functionName = FunctionHelper::getName($file, $pointer);

        if ($typeHint !== null && $typeHint->isNullable()) {
            $file->addError(
                sprintf('Return type hint allows null', $functionName),
                $pointer,
                self::METHOD_ID_GETTER_NOTNULL
            );
            return;
        }

        if ($annotation !== null && preg_match(self::NULLABLE_VAR_ANNOTATION_REGEX, $annotation->getContent())) {
            $file->addError(
                sprintf('Return annotation allows returning null', $functionName),
                $pointer,
                self::METHOD_ID_GETTER_NOTNULL
            );
            return;
        }
    }

    private function extractNonNullableProperties(File $file, int $pointer): void
    {
        $tokens = $file->getTokens();
        $endPointer = 0;
        $annotations = AnnotationHelper::getAnnotations($file, $pointer);
        $isNullable = false;
        $hasVarAnnotation = false;
        foreach ($annotations as $annotationName => $annotationsByName) {
            foreach ($annotationsByName as $annotation) {
                if ($annotationName === '@var') {
                    $hasVarAnnotation = true;

                    if (preg_match(self::NULLABLE_VAR_ANNOTATION_REGEX, $annotation->getContent())) {
                        $isNullable = true;
                    }
                }

                /** @var Annotation $annotation */
                if ($annotation->getEndPointer() > $endPointer) {
                    $endPointer = $annotation->getEndPointer();
                }
            }
        }
        if ($hasVarAnnotation && !$isNullable) {
            $propertyPointer = TokenHelper::findNext($file, [T_VARIABLE], $endPointer);

            $propertyToken = $tokens[$propertyPointer];

            if (!PropertyHelper::isStaticProperty($file, $propertyPointer) && $propertyToken['content'] != '$id') {
                $this->nonNullableProperties[$propertyPointer] = $propertyToken['content'];
            }
        }
    }

}
