<?php

declare (strict_types=1);

namespace SmartThingsCodingStandard\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use SlevomatCodingStandard\Helpers\Annotation;
use SlevomatCodingStandard\Helpers\AnnotationHelper;
use SlevomatCodingStandard\Helpers\ClassHelper;
use SlevomatCodingStandard\Helpers\DocCommentHelper;
use SlevomatCodingStandard\Helpers\FunctionHelper;
use SlevomatCodingStandard\Helpers\TokenHelper;

class ClassNamingSniff implements Sniff
{
    public const TRAIT_NAME_SUFFIX = 'TraitNameSuffix';

    /**
     * @return string[]
     */
    public function register(): array
    {
        return [
            T_TRAIT,
        ];
    }

    /**
     * @param int $pointer
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint
     */
    public function process(File $file, $pointer): void
    {
        $tokens = $file->getTokens();
        $token = $tokens[$pointer];

        // trait suffix check
        $className = ClassHelper::getName($file, $pointer);

        switch ($token['code']) {
            case T_TRAIT:
                if (substr($className, -5) !== 'Trait') {
                    $fix = $file->addFixableError(
                        sprintf('Trait name should have suffix `Trait`. Not found in %s', $className),
                        $pointer,
                        self::TRAIT_NAME_SUFFIX
                    );

                    if ($fix) {
                        $traitNamePointer = TokenHelper::findNext($file, [T_STRING], $pointer);
                        $traitNameToken = $tokens[$traitNamePointer];
                        $file->fixer->beginChangeset();
                        $file->fixer->replaceToken(
                            $traitNamePointer,
                            $traitNameToken['content'] . 'Trait'
                        );
                        $file->fixer->endChangeset();
                    }
                }
                break;
            default:
                throw new \Exception('unhandled token type ' . $token['code']);
        }
    }

}
